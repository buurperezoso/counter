import logo from './logo.svg';
import './App.css';
import Actions from './screens/Actions';
import Counter from './screens/Counter';

const App = () => {
  return (
    <div className="App">
      <div>
        <Counter />
        <Actions />
      </div>
    </div>
  );
}

export default App;
