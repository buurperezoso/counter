import React from 'react';
import styles from '../../styles/Counter.module.css';

const Counter = ({ counter }) => {
    return (
        <span className={styles.counterSpan}>Contador: {counter}</span>
    )
};

export default Counter;