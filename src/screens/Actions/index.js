import React from 'react';
import styles from '../../styles/Actions.module.css';

const Actions = ({ restar, sumar }) => {
    return (
        <div className={styles.actionsContainer}>
            <button type="button">Restar</button>
            <button type="button">Sumar</button>
        </div>
    )
};

export default Actions;